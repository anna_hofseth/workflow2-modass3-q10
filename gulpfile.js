const gulp = require('gulp');
const watchLess = require('gulp-watch-less');
const less = require('gulp-less');
const browserSync = require('browser-sync');
const imagemin = require('gulp-imagemin');

gulp.task('less', function(){
	gulp.src('less/*.less')
		.pipe(watchLess('less/*.less'))
		.pipe(less())
		.pipe(gulp.dest('css'));
});

gulp.task('browser-sync', function(){
	browserSync.init(['css/*.css'], {
		server: {
			baseDir: './'
		}
	});
});

gulp.task('minify-images', function(){
	gulp.src('original-images/*.jpg')
	.pipe(imagemin())
	.pipe(gulp.dest('optimized-images'))
});

gulp.task('default', ['less', 'browser-sync', 'minify-images'], function(){
	gulp.watch('less/*.less', ['less']);
})